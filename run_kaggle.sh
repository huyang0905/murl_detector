#!/bin/bash

echo "Build"
./build.sh

echo "Decompress data"
rm -rf data
mkdir data
unzip data_kaggle.zip
python trans.py
rm data.csv
mv training_raw.txt ./data

echo "Graph embedding"
./graph.sh