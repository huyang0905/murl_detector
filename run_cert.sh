#!/bin/bash

echo "Build"
./build.sh

echo "Decompress data"
rm -rf data
mkdir data
unzip -d ./data data_cert.zip

echo "Graph embedding"
./graph.sh