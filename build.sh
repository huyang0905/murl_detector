#!/bin/bash

echo "Compile GSL Lib"
mkdir gsl
cd ./gsl
wget ftp://ftp.gnu.org/gnu/gsl/gsl-2.3.tar.gz
tar -xvzf gsl-2.3.tar.gz
cd gsl-2.3
./configure
make -j4
make install
#sudo make install 
rm -r gsl
cd ../..

echo "Compile line src"
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib
export CFLAGS="-I/usr/local/include"
export LDFLAGS="-L/usr/local/lib"

git clone https://github.com/tangjianpku/LINE.git
cd LINE/linux
g++ -lm -pthread -Ofast -march=native -Wall -funroll-loops -ffast-math -Wno-unused-result line.cpp -o line -lgsl -lm -lgslcblas
g++ -lm -pthread -Ofast -march=native -Wall -funroll-loops -ffast-math -Wno-unused-result reconstruct.cpp -o reconstruct
g++ -lm -pthread -Ofast -march=native -Wall -funroll-loops -ffast-math -Wno-unused-result normalize.cpp -o normalize
g++ -lm -pthread -Ofast -march=native -Wall -funroll-loops -ffast-math -Wno-unused-result concatenate.cpp -o concatenate
cd ../
mkdir bin
mv ./linux/line ./bin/line
mv ./linux/reconstruct ./bin/reconstruct
mv ./linux/normalize ./bin/normalize
mv ./linux/concatenate ./bin/concatenate
export PATH=$(pwd)/bin:$PATH
cd ..