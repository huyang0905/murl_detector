"""
Raw Data Processor

Read raw records from dataset files

"""


# Author: Yang Hu <huyang0905@gmail.com>
#
# License: GPL v3

def analyze_data_file(file_path_list, max_length=10000):
    records = []
    for data_file_path in file_path_list:
        file = open(data_file_path, "r", encoding="utf-8")
        for line in file:
            if len(line) > max_length:
                line = line[:max_length]
            line = line.lower()
            sl = line.split(",")
            domain_name_list = sl[2].split("/")
            domain_name_list.extend(domain_name_list[0].split("."))
            key_value_pair_list = sl[3].split("&")
            record_map = {}
            if len(domain_name_list) > 0:
                record_map["d"] = domain_name_list
            else:
                record_map["d"] = []
            key_list = []
            for key_value_pair in key_value_pair_list:
                seg_list = key_value_pair.split("=")
                for key in seg_list:
                    if len(key) > 0:
                        key_list.append(key)
            record_map["k"] = key_list
            record_map["n"] = sl[0]
            record_map["l"] = sl[1]
            records.append(record_map)
        file.close()
    return records
