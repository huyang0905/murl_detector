"""
Model Training and Evaluation

This module consists of methods for training 
the RandomForest classifier and doing 10-fold
cross validation on it.

"""
# Author: Yang Hu <huyang0905@gmail.com>
#
# License: GPL v3

import os
import numpy
import pickle
from sklearn.ensemble import RandomForestClassifier
from sklearn.externals import joblib
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import cross_val_score


def score(estimator, x, y):
    """
    This score method computes f1 score for negative samples
    """
    y_pred = estimator.predict(x)
    tn, fp, fn, tp = confusion_matrix(y, y_pred).ravel()
    a = 2 * tn / (2 * tn + fn + fp)
    b = 2 * tp / (2 * tp + fn + fp)
    if a < b:
        return a
    else:
        return b


def train_model(training_file_path, n_cpu=4):
    """
    Train and evaluate the classifier
    """
    # Read dataset from training.txt
    f = open(training_file_path, "r", encoding="utf-8")
    data = numpy.loadtxt(f)
    f.close()
    X = data[:, 0:-1]
    y = data[:, -1]

    # Create a random forest classifier
    rf = RandomForestClassifier(class_weight="balanced", n_estimators=200, n_jobs=n_cpu)

    # Conduct cross validation
    scores = cross_val_score(rf, X, y, cv=6, scoring=score, n_jobs=n_cpu)
    print("Mean f1 score: " + ("%.3f" % scores.mean()))
    rf.fit(X, y)
    return rf

def predict(model, fv_map):
    """
    Detect malicious url based on trained model
    """
    for id, fv_str in fv_map.items():
        fv = [str(atom_str) for atom_str in fv_str.split()]
        if model.predict([fv])[0] == 0:
            print(id, "normal")
        elif model.predict([fv])[0] == 1:
            print(id, "malicious")



if __name__ == '__main__':
    s_directory = "./data"
    training_file_path = s_directory + "/training.txt"

    print("Cross Validation\n")
    tm = train_model(training_file_path)

    model_dir_path = os.getcwd() + "/model"
    if not os.path.isdir(model_dir_path):
        os.mkdir(model_dir_path)
    print("Save the trained model to " + model_dir_path)
    joblib.dump(tm, model_dir_path + "/tm.model")

    print("Predict")
    with open(s_directory + "/fv_map.pickle", "rb") as handle:
        fv_map = pickle.load(handle)
        predict(tm, fv_map)

