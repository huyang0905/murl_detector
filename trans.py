"""
Data Transformer

Tranform kaggle data to match the format we defined 

"""
# Author: Yang Hu <huyang0905@gmail.com>
#
# License: GPL v3


import os
from urllib.parse import quote


def trans_kaggle_file(file_input_path, file_output_path):
    with open(file_input_path, "r") as fr:
        with open(file_output_path, "w") as fw:
            i = 500000
            for line in fr:
                if "url,label" in line:
                    continue
                domain_name_str = ""
                paras_str = ""
                type_abbr_str = ""

                atom_list = line.split(",")
                if len(atom_list) == 2:
                    cell_list = atom_list[0].split("?")
                    domain_name_str = cell_list[0]
                    if len(cell_list) > 1:
                        paras_str = cell_list[1]
                    type_str = atom_list[1]
                    if type_str == "bad\n":
                        type_abbr_str = "w"
                    elif type_str == "good\n":
                        type_abbr_str = "n"
                    else:
                        print("No such type", type_str)
                        print(line)
                else:
                    atom_list = line.split("\"")
                    cell_list = atom_list[1].split("?")

                    domain_name_str = quote(cell_list[0], 'utf-8')
                    if "," in domain_name_str:
                        print(domain_name_str)
                    if len(cell_list) > 1:
                        paras_str = cell_list[1]
                    type_str = atom_list[2][1:]
                    if type_str == "bad\n":
                        type_abbr_str = "w"
                    elif type_str == "good\n":
                        type_abbr_str = "n"
                    else:
                        print("No such type", type_str)
                        print(line)
                fw.write(",".join([str(i), type_abbr_str, domain_name_str, paras_str]))
                fw.write("\n")
                i += 1


if __name__ == '__main__':
    trans_kaggle_file(os.getcwd() + "/data.csv", os.getcwd() + "/training_raw.txt")
