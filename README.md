# Malicious URL Detector
This tool utilizes graph embedding and machine learning techniques to detect malicious urls. This tool was ranked 21/163 in 2017 Network Security Technology Challenge held by CNCERT/CC.

## Quick Start
At first, you should set up the experimental environment. It is recommended to build our Dockerfile, which automatically installs necessary software packages, or you can manually install those packages such as `Anaconda 3`, `git`, `gcc`, `g++` and `wget`.

Then you can execute the `run_cert.sh`, which generates a machine learning model trained on the dataset in `data_cert.zip`. You can also execute `run_kaggle.sh` for `data_kaggle.zip`, but since `data_kaggle.zip` is much larger than `data_cert.zip`, you'd better execute `run_kaggle.sh` on a high-performance machine with enough cores, thereby getting the trained model faster (if your machine has many available cores, please modify `cpu_num` variable in `graph.py` so that our tool can utilize those cores).

## Dataset
Our tool was evaluated on two datasets. One dataset in `data_cert.zip` was provided by [CNCERT/CC](https://www.kesci.com/apps/home/competition/58dcbcb26fe39379f16f04a2/content/2), while the dataset in `data_kaggle.zip` were obtained from [Kaggle](https://www.kaggle.com/antonyj453/urldataset). For the ease of processing, our tool tranforms the two datasets into one uniform format, where each record consists of four parts: ID, Normal (n) / Malicious (m), Domain Name (CNCERT/CC did data masking on all domain names in `data_cert.zip`).

## Detection Logic Construction
Our tool transferred the urls into a directed graph. For example, consider the url `example.com?a=1&b=2`, we produce nodes to represent the whole url and its components like `example`, `com`, `a`, `1`, `b` and `2`. We draw an edge from node A to node B if B is the component of A. This way, all urls can be represented in a very large directed graph. Then we use graph embedding techniques to generate feature vectors for each node. Finally, we use those feature vectors train and evaluate our machine learning models. 

## License
[GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.html)

## Author

Yang Hu huyang0905@gmail.com
