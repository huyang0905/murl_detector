#!/bin/bash

echo "Construct graph"
python3 ./graph.py

echo "Graph embedding"
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib
export CFLAGS="-I/usr/local/include"
export LDFLAGS="-L/usr/local/lib"
export PATH=$(pwd)/LINE/bin:$PATH
line -train ./data/graph.txt -output ./data/embedding_features.txt -binary 0 -size 200 -order 1 -negative 2 -samples 5 -threads 4
echo "Read Fecture Vectors"
python3 ./feature.py
echo "Train and test"
python3 ./train.py
echo "Finished"