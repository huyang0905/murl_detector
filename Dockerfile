FROM ubuntu:18.04
MAINTAINER Yang Hu <huyang0905@gmail.com>

# Install basic dependencies
RUN apt-get update && apt-get install -y --no-install-recommends pkg-config git unzip python-dev gcc g++ wget vim ca-certificates

# Install anaconda for python 3.6
RUN mkdir /home/huyang
RUN cd /home/huyang/ && wget --no-check-certificate https://repo.anaconda.com/archive/Anaconda3-5.3.0-Linux-x86_64.sh && chmod 777 ./Anaconda3-5.3.0-Linux-x86_64.sh && ./Anaconda3-5.3.0-Linux-x86_64.sh -b -p /home/huyang/conda
RUN echo "export PATH=/home/huyang/conda/bin:$PATH" >> ~/.bashrc
RUN export PATH=/home/huyang/conda/bin:$PATH

# Set workdir
WORKDIR /home/huyang

# clone code
RUN git clone https://yhu0905@bitbucket.org/yhu0905/murl_detector.git
