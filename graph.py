"""
Graph Generation

This module transforms urls into a graph description file,
which the graph embedding tool can recognize and conduct 
graph embedding.

"""
# Author: Yang Hu <huyang0905@gmail.com>
#
# License: GPL v3


import hashlib
import pickle
from difflib import SequenceMatcher
from multiprocessing import Pool

from da import analyze_data_file


def count_process(record_group):
    """
    Counting entities in a process
    """
    sub_dict = {}
    for record_map in record_group:
        entity_list = []
        for entity in record_map["d"]:
            entity_list.append(entity)
        for entity in record_map["k"]:
            entity_list.append(entity)

        for entity in entity_list:
            if entity not in sub_dict.keys():
                sub_dict[entity] = [0, 0, 0]
            if record_map['l'] == 'n':
                sub_dict[entity][0] += 1
            elif record_map['l'] == 'w':
                sub_dict[entity][1] += 1
            else:
                sub_dict[entity][2] += 1
    return sub_dict


def get_statistics_dict(records, cpu_num=8):
    """
    Get statistic results in parallel
    """
    record_groups = []
    for i in range(cpu_num):
        record_groups.append([])
    c = 0
    for record_map in records:
        record_groups[c % cpu_num].append(record_map)
        c = c + 1
    # Create process pool to speed up counting
    pool = Pool(processes=cpu_num)
    dict_list = pool.map(count_process, record_groups)
    pool.close()
    pool.join()

    # Combine sub dict into one dict
    statistics_dict = {}
    for one_dict in dict_list:
        for (key, value) in one_dict.items():
            if key not in statistics_dict.keys():
                statistics_dict[key] = [0, 0, 0]
            statistics_dict[key][0] += value[0]
            statistics_dict[key][1] += value[1]
            statistics_dict[key][2] += value[2]
    return statistics_dict


def combine_entities(records, threshold=0.4):
    """
    Combine statistic results from different processors.
    """
    statistics_dict = get_statistics_dict(records, cpu_num=8)

    # Remove rarely-used or indistinguished entities
    diff_dict = {}
    fre_dict = {}
    for key, values in statistics_dict.items():
        if values[0] == 0 and values[1] == 0:
            continue
        diff_dict[key] = values[1] / (values[0] + values[1])
        fre_dict[key] = values[0] + values[1] + values[2]
    filtered_entity_list = []
    for key, value in diff_dict.items():
        if value >= threshold:
            filtered_entity_list.append(key)
    return filtered_entity_list


def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()


def construct_graph(records, graph_output_path):
    """
    Generate the graph description file
    """
    # Get filtered entities
    filtered_entity_list = combine_entities(records)

    output_file = open(graph_output_path, "w", encoding="utf-8")
    s = 0
    for record_map in records:
        # Print progress
        s = s + 1
        print(s, "/", len(records))

        # Compute the hash value of the root node
        id = record_map["n"]
        hash1 = hashlib.md5()
        hash1.update(id.encode('utf-8'))
        node_root = hash1.hexdigest()
        domain_name_list = record_map["d"]
        key_list = record_map["k"]
        current_entity_list = []
        for entity in domain_name_list:
            current_entity_list.append(entity)
        for entity in key_list:
            current_entity_list.append(entity)

        # Compute the hash value of leaf nodes
        for entity in current_entity_list:
            if entity in filtered_entity_list:
                hash1 = hashlib.md5()
                hash1.update(entity.encode('utf-8'))
                node_son = hash1.hexdigest()
                output_file.write(node_son + " " + node_root + " " + "1\n")
                output_file.write(node_root + " " + node_son + " " + "1\n")
        hash1 = hashlib.md5()
        hash1.update("node_son".encode('utf-8'))
        node_son = hash1.hexdigest()
        output_file.write(node_son + " " + node_root + " " + "0\n")
        output_file.write(node_root + " " + node_son + " " + "0\n")
    output_file.close()


if __name__ == '__main__':
    s_directory = "./data"
    s_file_raw = s_directory + "/training_raw.txt"
    s_urls_raw = "./urls"
    file_path_list = [s_file_raw, s_urls_raw]
    s_file_graph = s_directory + "/graph.txt"

    # Get records from file
    records = analyze_data_file(file_path_list)
    construct_graph(records, s_file_graph)

    # Serialize records 
    with open(s_directory + "/records.pickle", "wb") as handle:
        pickle.dump(records, handle, protocol=pickle.HIGHEST_PROTOCOL)
